package com.example.tideh.androidplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.session.MediaSessionManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.data;

public class MainActivity extends Activity implements OnCompletionListener, SeekBar.OnSeekBarChangeListener {


    public ImageButton play, next, prev;
    public Button playlist;
    public SeekBar songProgressBar;
    public TextView songTitle, songCurrentDuration, songTotalDuration;
    private MediaPlayer mediaPlayer;
    private Handler mHandler = new Handler();
    private SongsManager songManager;
    private Utilites utils;
    private int currentSongIndex = 0;

    AudioManager audioManager;

    AFListener afListenerMusic;

    int thisSong;


    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

    final static String LOG_TAG = "focus";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player);

        play = (ImageButton) findViewById(R.id.imageButton2);
        next = (ImageButton) findViewById(R.id.imageButton4);
        prev = (ImageButton) findViewById(R.id.imageButton3);
        playlist = (Button) findViewById(R.id.btnPlaylist);
        songTitle = (TextView) findViewById(R.id.songTitle);

        songProgressBar = (SeekBar) findViewById(R.id.seekBar);

        songCurrentDuration = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDuration = (TextView) findViewById(R.id.songTotalDurationLabel);


        mediaPlayer = new MediaPlayer();
        songManager = new SongsManager(this);
        utils = new Utilites();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        afListenerMusic = new AFListener(mediaPlayer);


        songProgressBar.setOnSeekBarChangeListener(this);
        mediaPlayer.setOnCompletionListener(this);


        songsList = songManager.getPlayList();

        playSong(0);

        play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mediaPlayer.isPlaying()) {
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                    }
                } else {
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                    }
                }

            }
        });


        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (currentSongIndex < (songsList.size() - 1)) {
                    thisSong = playSong(currentSongIndex + 1);
                    currentSongIndex = currentSongIndex + 1;
                } else {
                    thisSong = playSong(0);
                    currentSongIndex = 0;
                }

            }
        });


        prev.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (currentSongIndex > 0) {
                    currentSongIndex = currentSongIndex - 1;
                    thisSong = playSong(currentSongIndex);
                } else {
                    currentSongIndex = songsList.size() - 1;
                    thisSong = playSong(currentSongIndex);
                }

            }
        });

        playlist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), PlayListActivity.class);
                startActivityForResult(i, 100);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            currentSongIndex = data.getExtras().getInt("songIndex");
            thisSong = playSong(currentSongIndex);
        }

    }


    public int playSong(int songIndex) {

        try {

            if (thisSong != songIndex) {
                mediaPlayer.reset();
            }
            mediaPlayer.setDataSource(songsList.get(songIndex).get("songPath"));
            mediaPlayer.prepare();


            audioManager.requestAudioFocus(afListenerMusic,
                    AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

            mediaPlayer.start();

            String title = songsList.get(songIndex).get("songTitle");
            songTitle.setText(title);

            songProgressBar.setProgress(0);
            songProgressBar.setMax(100);

            updateProgressBar();

            Intent serviceIntent = new Intent(getApplicationContext(), MediaPlayerService.class);
            serviceIntent.setAction( MediaPlayerService.ACTION_PLAY);
            serviceIntent.putExtra("title", title);
            startService( serviceIntent);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return songIndex;
    }


    public void updateProgressBar() {
        //Обновление прогрессбара раз в сто милисекунд
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }


    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();

            songTotalDuration.setText("" + utils.milliSecondsToTimer(totalDuration));

            songCurrentDuration.setText("" + utils.milliSecondsToTimer(currentDuration));

            int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));

            songProgressBar.setProgress(progress);

            mHandler.postDelayed(this, 100);
        }
    };


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mediaPlayer.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        mediaPlayer.seekTo(currentPosition);

        updateProgressBar();
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {

        if (currentSongIndex < (songsList.size() - 1)) {
            playSong(currentSongIndex + 1);
            currentSongIndex = currentSongIndex + 1;
        } else {
            playSong(0);
            currentSongIndex = 0;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
    }

    class AFListener implements AudioManager.OnAudioFocusChangeListener {

        MediaPlayer mp;

        public AFListener(MediaPlayer mp) {
            this.mp = mp;
        }

        @Override
        public void onAudioFocusChange(int focusChange) {
            String event = "";
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                    event = "AUDIOFOCUS_LOSS";
                    mp.pause();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    event = "AUDIOFOCUS_LOSS_TRANSIENT";
                    mp.pause();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    event = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
                    mp.setVolume(0.5f, 0.5f);
                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                    event = "AUDIOFOCUS_GAIN";
                    if (!mp.isPlaying())
                        mp.start();
                    mp.setVolume(1.0f, 1.0f);
                    break;
            }
            Log.d(LOG_TAG, " onAudioFocusChange: " + event);
        }
    }
}